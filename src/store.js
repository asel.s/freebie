import {combineReducers, createStore} from "redux";
import slideReducer from "./reducer/slide-reducer";
// import signReducer from "./reducer/sign-reducer";
import signupReducer from "./reducer/signup-reducer";


let reducers = combineReducers({
    slideReducer,
    signUpReducer: signupReducer
    // signReducer
});

let store = createStore(reducers,
    // window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export default store;