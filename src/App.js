import React from 'react';
import './App.css';
import FirstSlider from "./components/sliders/slider-1/Slider-1";
import SecondSlider from "./components/sliders/slider-2/Slider-2";
import ThirdSlider from "./components/sliders/slider-3/Slider-3";
import {connect} from "react-redux";
import  {getIndex} from "./reducer/slide-reducer";
import Verification from "./components/confirm/verification/verification";
import Terms from "./components/confirm/terms/terms";
import MobileNumberOne from "./components/mobile-number/mobileNumber-1";
import MobileNumberTwo from "./components/mobile-number/mobileNumber-2";
import CreateCode from "./components/confirm/create-code/create-code";
import Login from "./components/login/login";
import About from "./components/about/aboutYou/about";
import Register from "./components/about/aboutForm/aboutForm";
import Accounts from "./components/accounts/accounts";
import AddingAccount from "./components/accounts/addingAccount/addingAccount";
import AddManualAccount from "./components/accounts/addingAccount/add-manual-account/addManualAccount";
import Successfull from "./components/successfull-added/successfull";
import AboutForm from "./components/about/aboutForm/aboutForm";


const App = (props) => {
    const onNextSlide = (e) => {
        if (e.currentTarget.dataset.direction === 'next') {
            props.getIndex(props.indexSlide + 1);
        } else if (e.currentTarget.dataset.direction === 'prev') {
            props.getIndex(props.indexSlide - 1);
        }
    }

    const componentsItem = [
        <FirstSlider onNextSlide={onNextSlide}/>,
        <SecondSlider onNextSlide={onNextSlide}/>,
        <ThirdSlider onNextSlide={onNextSlide}/>,
        <MobileNumberOne onNextSlide={onNextSlide}/>,
        <MobileNumberTwo onNextSlide={onNextSlide}/>,
        <CreateCode onNextSlide={onNextSlide}/>,
        <Verification onNextSlide={onNextSlide}/>,
        <Terms onNextSlide={onNextSlide} />,
        <Login onNextSlide={onNextSlide} />,
        <About onNextSlide={onNextSlide} />,
        <AboutForm onNextSlide={onNextSlide} />,
        <Accounts onNextSlide={onNextSlide} />,
        <AddingAccount onNextSlide={onNextSlide} />,
        <AddManualAccount onNextSlide={onNextSlide} />,
        <Successfull onNextSlide={onNextSlide} />,
    ];

    return (
        <div className="App">
            {componentsItem[props.indexSlide]}
        </div>
    );
}

let mapStateToProps = (state) => ({
    indexSlide: state.slideReducer.indexSlide
})

export default connect(mapStateToProps, {getIndex})(App);
