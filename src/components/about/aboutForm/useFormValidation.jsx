import React from "react";


function useFormValidation(initialState, validate) {
    const [values, setValues] = React.useState(initialState);
    const [errors, setErrors] = React.useState({});
    const [isSubmitting, setSubmitting] = React.useState(false);
    const [userField, setUserField] = React.useState(false);
    const [emailField, setEmailField] = React.useState(false);

    // React.useEffect(() => {
    //     if (isSubmitting) {
    //         const noErrors = Object.keys(errors).length === 0;
    //         if (noErrors) {
    //             setSubmitting(true);
    //         } else {
    //             setSubmitting(false);
    //         }
    //     }
    //
    // }, [errors]);

    React.useEffect(() => {
        if(values.userName.length > 6) {
            setUserField(true);
        } else {
            setUserField(false);
        }
    }, [values.userName]);

    React.useEffect(() => {
        if(/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
            setEmailField(true);
        } else {
            setEmailField(false);
        }

    }, [values.email]);

    React.useEffect(() => {
        if((/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) && (values.userName.length > 6)) {
            setSubmitting(true);
        } else {
            setSubmitting(false);
        }
    }, [errors]);



    function handleChange(event) {
        setValues({
            ...values,
            [event.target.name]: event.target.value
        });
    }

    function handleBlur() {
        const validationErrors = validate(values);
        setErrors(validationErrors);
    }

    function handleSubmit(event) {
        event.preventDefault();
        const validationErrors = validate(values);
        setErrors(validationErrors);
        setSubmitting(true);
    }

    return {
        handleSubmit,
        handleChange,
        handleBlur,
        values,
        errors,
        isSubmitting,
        userField,
        emailField
    };
}

export default useFormValidation;
