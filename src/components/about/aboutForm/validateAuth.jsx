export default function validateAuth(values) {
    let errors = {};
    // UserName Errors
    if (!values.email) {
        errors.email = "Required Email";
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
        errors.email = "Invalid email address";
    }
    // Email Errors
    if (!values.userName) {
        errors.userName = "Required user name";
    } else if (values.userName.length < 6) {
        errors.userName = "UserName must be at least 6 characters";
    }
    return errors;
}