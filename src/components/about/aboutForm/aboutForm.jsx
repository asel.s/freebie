import React, {useEffect} from "react";
import './aboutForm.css';

import useFormValidation from "./useFormValidation";
import validateAuth from "./validateAuth";
import arrow from "../../../assets/images/back-arrow.png";
import {getIndex} from "../../../reducer/slide-reducer";
import {connect} from "react-redux";


const INITIAL_STATE = {
    email: "",
    userName: ""
};

function AboutForm(props) {

    const {
        handleSubmit,
        handleChange,
        handleBlur,
        values,
        errors,
        isSubmitting,
        userField,
        emailField
    } = useFormValidation(INITIAL_STATE, validateAuth);

    return (
        <div className="wrapper">
            <div className="arrow-block">
                <img className="arrow" src={arrow} alt="" data-direction="prev" onClick={props.onNextSlide}/>
            </div>
            <h5 className="start">More about you</h5>
            <p className="about-subtitle">For delivery updates</p>
            <form onSubmit={handleSubmit}>
                <div className="field">
                    {userField ? <i className="fas fa-check"/> : ''}
                    <input
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.userName}
                        className={errors.userName && "error-input"}
                        name="userName"
                        type="text"
                        placeholder="First and last name"
                    />
                    {errors.userName && <p className="error-text">{errors.userName}</p>}
                </div>
                <div className="field">
                    {  emailField ? <i className="fas fa-check"/> : null }
                    <input
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="email"
                        value={values.email}
                        className={errors.email && "error-input"}
                        autoComplete="off"
                        placeholder="Your email address"
                    />
                    { errors.email && <p className="error-text">{errors.email}</p> }
                </div>
                <div className="field">
                    <input
                        type="text"
                        placeholder="Address"
                    />
                </div>
                <div className="btn-wrap">
                    <button disabled={!isSubmitting} type="submit"
                            className={!isSubmitting ? 'bg-gr-btn' : 'bg-green-btn'}
                            data-direction="next"
                            onClick={ props.onNextSlide }
                    >
                        Get started
                    </button>
                </div>
            </form>
        </div>
    );
}

let mapStateToProps = (state) => {
    return ({
        indexSlide: state.slideReducer.indexSlide,
    })
};

let mapDispatchToProps = (dispatch) => ({
    changeSlide: index => {
        console.log(index);
        dispatch(getIndex(index))
    },
})

export default connect(mapStateToProps, mapDispatchToProps)(AboutForm);