import React, {useState} from "react";
import arrow from "../../../assets/images/back-arrow.png";
import './about.css';
import pic from '../../../assets/images/pic.png'

const About = (props) => {

    const [image, setImage] = useState({preview: '', raw: ''});


    const handleChange = (e) => {
        const file = e.target.files[0];
        if (file) {
            const reader = new FileReader();

            reader.onload = e => {
                setImage({
                    preview: e.target.result,
                    raw: file
                })
            };
            reader.readAsDataURL(file);

        }
    }

    /* const handleUpload = (e) => {

             const formData = new FormData()
             formData.append('image', image.raw)

             const config = { header: {'content-type': 'multypart/form-data'} }

             await uploadToBackend('endpoint', {image: image.raw}, config)
         }
        const handleSubmit = (e) => {
            e.preventDefault();
    } */

    return <div className="wrapper">
        <div className="form-items">
            <div className="arrow-block">
                <img className="arrow" src={arrow} data-direction="prev" onClick={props.onNextSlide} alt="arrow"/>
            </div>
            <div className="input-from">
                <h5 className="start">More about you</h5>
                <p className="profile">Upload your profile picture</p>

                <div className="block-pic">
                    <label htmlFor="upload-button">
                        {
                            image.preview ?
                                <div>
                                    <img src={image.preview} className="profile-pic" alt="picture"/>
                                    <div className="upload-btn">
                                        <i className="fas fa-camera"/>
                                    </div>
                                </div>
                                : (
                                    <>
                                        <div className="mainPhoto">
                                            <img src={pic} className="profile-pic" alt="img"/>
                                        </div>
                                        <div className="upload-btn">
                                            <i className="fas fa-camera"/>
                                        </div>
                                    </>
                                )
                        }
                    </label>
                    <input type="file" id="upload-button" style={{display: 'none'}} onChange={handleChange}/>
                </div>

                <p className="decs">BTW, you look great:)</p>
            </div>
            <div className="block-btn-group">
                <button className="button btn-sz later" data-direction="prev" onClick={props.onNextSlide}>Maybe later</button>
                <button className="button btn-sz cont"
                    data-direction="next" onClick={props.onNextSlide}>
                    Continue
                </button>
            </div>
        </div>
    </div>
}

export default About;