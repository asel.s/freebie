import React from "react";
import {connect} from "react-redux";
import {getIndex} from "../../reducer/slide-reducer";
import arrow from "../../assets/images/back-arrow.png";
import './mobile-number.css'
import MaskedInput from "react-text-mask";
import Button from "../button/button";


const MobileNumber1 = (props) => {

    return <div className="wrapper">
        <div className="arrow-block">
            <img className="arrow" src={arrow} alt="" data-direction="prev" onClick={props.onNextSlide}/>
        </div>
        <h5 className="start">Let's get started</h5>
        <p className="l-phone">What is your mobile number?</p>
        <MaskedInput type="text" placeholder="Mobile number" className="mobile-number"
                     mask={['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/]}
                     guide={true}
                     data-direction="next"
                     onClick={props.onNextSlide}
        />
        <Button onClick={props.onNextSlide}/>
    </div>
}
let mapStateToProps = (state) => ({
    indexSlide: state.slideReducer.indexSlide
})

export default connect(mapStateToProps, {getIndex})(MobileNumber1);