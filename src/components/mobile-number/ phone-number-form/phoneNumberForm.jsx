import React from "react";
import './phoneNumberForm.css';
import {connect} from "react-redux";
import MaskedInput from 'react-text-mask'


const PhoneNumberFrom = (props) => {
    return (
        <div>
                <MaskedInput type="text" placeholder="Mobile number" className=" form-control form-mobile-number"
                           mask={['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/]}
                           guide={true}
                           />
        </div>
    )
}

export default connect()(PhoneNumberFrom);