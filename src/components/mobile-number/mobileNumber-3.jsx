import React, {useEffect, useState} from "react";
import './mobile-number.css';
import 'react-phone-input-2/lib/style.css';
import {connect} from "react-redux";
import arrow from "../../assets/images/back-arrow.png";
import MaskedInput from "react-text-mask";
import Button from "../button/button";


const MobileNumberThree = (props) => {

    return (
        <div>
            <div className="wrapper">
                <div className="arrow-block">
                    <img className="arrow" src={arrow} alt="" data-direction="prev" onClick={props.onNextSlide}/>
                </div>
                <div className="input-from">
                    <h5 className="start">Let's get started</h5>
                    <p className="l-phone">What is your mobile number?</p>
                    <MaskedInput type="text" placeholder="Mobile number" className="form-control form-mobile-number"
                                 mask={['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/]}
                                 guide={true}
                    />
                </div>
                <Button onClick={props.onNextSlide} />
            </div>

        </div>
    )
}

let mapStateToProps = (state) => {
    return ({
        phone: state.signUpReducer.phone
    });
}

export default connect(mapStateToProps, {})(MobileNumberThree);
