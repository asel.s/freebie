import React, { useState} from "react";
import './mobile-number.css';
import 'react-phone-input-2/lib/style.css';
import PhoneNumberInput from "./phone-number-input/phone-number-input";
import {connect} from "react-redux";
import arrow from "../../assets/images/back-arrow.png";
import MaskedInput from 'react-text-mask';
import phoneNumber from "react-native-phone-input/lib/phoneNumber";
import {clearedPhoneNumber, setPhoneNumber} from "../../reducer/signup-reducer";

const MobileNumberTwo = (props) => {

    const numberPhoneStr = props.phoneNumber.join();

    return (
        <div>
            <div className="wrapper">
                <div className="arrow-block">
                    <img className="arrow" src={arrow} alt="" data-direction="prev" onClick={props.onNextSlide}/>
                </div>
                <div className="input-from">
                    <h5 className="start">Let's get started</h5>
                    <p className="l-phone">What is your mobile number?</p>
                    <MaskedInput type="text" placeholder="Mobile number" className="mobile-number"
                                 mask={['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/]}
                                 guide={true}
                                 value={numberPhoneStr}
                    />
                </div>
            </div>
            <div className="keypad-block">
                <PhoneNumberInput onNextSlide={props.onNextSlide}
                                             setPhoneNumber={props.setPhoneNumber}
                                             phoneNumber={props.phoneNumber}
                                             clearedPhoneNumber={props.clearedPhoneNumber}

                />
            </div>
        </div>
    )
}

let mapStateToProps = (state) => {
    return ({
        phoneNumber: state.signUpReducer.phone
    });
}

export default connect(mapStateToProps, {setPhoneNumber, clearedPhoneNumber})(MobileNumberTwo);
