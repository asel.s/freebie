import React from "react";
import './phone-number-input.css';
import {connect} from "react-redux";


const PhoneNumberInput = (props) => {

    const onInputNum = (e) => {
        props.setPhoneNumber(e.currentTarget.textContent);
    }
    const onBackspace = () => {
        props.clearedPhoneNumber();
    }
    const isEnabled = props.phoneNumber.length > 9;

    return <div className="phone-number bg-color">
        <div className="row row-1">
            <div onClick={onInputNum} className="btn-num btn-num-wh active">1</div>
            <div onClick={onInputNum} className="btn-num btn-num-wh active">2</div>
            <div onClick={onInputNum} className="btn-num btn-num-wh active">3</div>
        </div>
        <div className="row row-2">
            <div onClick={onInputNum} className="btn-num btn-num-wh active">4</div>
            <div onClick={onInputNum} className="btn-num btn-num-wh active">5</div>
            <div onClick={onInputNum} className="btn-num btn-num-wh active">6</div>
        </div>
        <div className="row row-3">
            <div onClick={onInputNum} className="btn-num btn-num-wh active">7</div>
            <div onClick={onInputNum} className="btn-num btn-num-wh active">8</div>
            <div onClick={onInputNum} className="btn-num btn-num-wh active">9</div>
        </div>
        <div className="row row-4">
            <div onClick={onInputNum} className="btn-num btn-num-wh"></div>
            <div onClick={onInputNum} className="btn-num btn-num-wh active">0</div>
            <div onClick={onBackspace} className="btn-num btn-num-wh active"><i className="fas fa-backspace"/></div>
        </div>
        <div className="btn-item">
            <button
                disabled={!isEnabled}
                data-direction="next" onClick={props.onNextSlide}
                className="button btn-green"
                style={{background: isEnabled ? '' : '#ccc'}}
            >Continue
            </button>
        </div>
    </div>
}

export default connect()(PhoneNumberInput);

