import React from "react";
import content from '../../assets/images/content.png'
import Avatar from'../../assets/images/Avatar.png'
const Accounts = (props) => {
    return (
        <div className="wrapper">
            <div className="header">
                <div>
                    <img src={Avatar} alt=""/>
                </div>
                <div>
                    <h5 className="start">Accounts</h5>
                </div>
                <div className="plus">
                    <i className='fa fa-plus'/>
                </div>
            </div>
            <ul className="list">
                <li>Savings 2020</li>
                <li className="active-li">Checkings 2020</li>
                <li>My wallet 2020</li>
            </ul>
            <div className="block-accounts">
                <p>You don’t have set any account
                    Create one in seconds.</p>
                <img src={content} alt=""/>
                <button data-direction="next" onClick={props.onNextSlide} className="button btn-green" >Add account</button>
            </div>
        </div>
    )
}

export default Accounts;