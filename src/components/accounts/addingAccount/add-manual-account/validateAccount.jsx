export default function validateAccount(values) {
    let errors = {};
    //Account errors
    if (!values.account) {
        errors.account = "Required user name";
    }
    //AccountName errors
    if (!values.accountName) {
        errors.accountName =  "Required accountName";
    } else  if (values.accountName < 6) {
        errors.accountName = "accountName must be at least 6 characters";
    }
    // Balance errors
    if (!values.balance) {
        errors.balance =  "Required balance";
    }
    else if (/^\d+(?:\.\d{0,2})$/.test(errors.balance)) {
        errors.balance = "Invalid balance";
    }
    //currency errors
    if (!values.currency) {
        errors.currency = "Required currency";
    }

    return errors;
}


