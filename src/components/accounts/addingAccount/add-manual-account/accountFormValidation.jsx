import React from "react";



const AccountFormValidation = (initialState, validate) => {
    const [values, setValues] = React.useState(initialState);
    const [errors, setErrors] = React.useState({});
    const [isEnable, setisEnable] = React.useState(false);

   // React.useEffect(() => {
   //          const noErrors = Object.keys(errors).length === 0;
   //          if (noErrors) {
   //              setIsEnable(false);
   //          } else {
   //              setIsEnable(false);
   //          }
   //  }, [errors]);

    React.useEffect(() => {

        if (/(?:^\d{1,3}(?:\.?\d{3})*(?:,\d{2})?$)|(?:^\d{1,3}(?:,?\d{3})*(?:\.\d{2})?$)/.test(values.balance)) {
            setisEnable(true);
        } else {
            setisEnable(false);
        }
    }, [values.balance]);

    function handleChange(e) {
        setValues({
          ...values,
          [e.target.name]: e.target.value
      });
    }

    function handleBlur() {
        const validationErrors = validate(values);
        setErrors(validationErrors);
    }

    function handleSubmit (e) {

        e.preventDefault();
        const validationErrors = validate(values);
        setErrors(validationErrors);
        isEnable(true);

    }

    return {
        handleChange,
        handleBlur,
        handleSubmit,
        errors,
        values,
        isEnable
    }
}

export default AccountFormValidation;