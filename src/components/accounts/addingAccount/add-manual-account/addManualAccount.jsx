import React, {useEffect} from "react";
import './addManualAccount.css'
import AccountFormValidation from "./accountFormValidation";
import validateAccount from "./validateAccount";
import {getIndex} from "../../../../reducer/slide-reducer";
import {connect} from "react-redux";

const INITIAL_STATE = {
    account: "",
    accountName: "",
    balance: "",
    currency: "",
}

const AddManualAccount = (props) => {
    const {
        handleChange,
        handleSubmit,
        handleBlur,
        errors,
        values,
        isEnable
    } = AccountFormValidation(INITIAL_STATE, validateAccount);

    return (
        <div className="wrapper">
            <form className="form-group" onSubmit={handleSubmit}>
                <select name="account"
                        placeholder="Select an account type"
                        onChange={handleChange}>
                    <option value="savings">Savings</option>
                    <option value="salary">Salary</option>
                </select>
                <div className="block-accountName">
                    <input
                        type="text"
                        placeholder="Account name"
                        name="accountName"
                        onChange={handleChange}
                        value={values.accountName}
                        onBlur={handleBlur}
                    />
                    {errors.accountName && <p className="error-text">{errors.accountName}</p>}
                </div>
                <div className="block-balance">
                    <input
                        type="text"
                        placeholder="Current balance"
                        name="balance"
                        onChange={handleChange}
                        value={values.balance}
                        onBlur={handleBlur}
                    />
                    {errors.balance && <p className="error-text">{errors.balance}</p>}
                </div>
                <select name="currency" id="" onChange={handleChange}>
                    <option value="USD">$USD - United States Dollar</option>
                    <option value="GBP">Grate Britane pounds</option>
                    <option value="BRL">KGZ</option>
                    <option value="KG">BRL</option>
                    <option value="MXN">MXN</option>
                </select>
                <button
                    disabled={!isEnable}
                    type="submit"
                    data-direction="next"
                    onClick={props.onNextSlide}
                    style={{background: !isEnable ? '#ccc' : ''}}
                >Continue
                </button>
            </form>
        </div>
    )
}

let mapStateToProps = (state) => {
    return ({
        indexSlide: state.slideReducer.indexSlide,
    })
};

let mapDispatchToProps = (dispatch) => ({
    changeSlide: index => {
        dispatch(getIndex(index))
    },
})

export default connect(mapStateToProps, mapDispatchToProps)(AddManualAccount);