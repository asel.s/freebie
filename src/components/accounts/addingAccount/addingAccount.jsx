import React, {useState} from "react";
import './addingAccount.css';
import {connect} from "react-redux";
import {getIndex} from "../../../reducer/slide-reducer";

const AddingAccount = (props) => {

    const [checked, setChecked] = useState(false);

    return (
        <div className="wrapper">
            <div className="arrow-block">
                <span data-direction="prev" onClick={props.onNextSlide}>cancel</span>
            </div>
            <h5 className="start">Add account</h5>
            <div className="block-choose">
                <input
                    type="radio"
                    id="radio-1"
                    style={{display: 'none'}}
                    onChange={(e) => setChecked(e.target.value)}
                    value="Bank account"
                    name="radios"
                />
                <label className="wrap" htmlFor="radio-1">
                    <div className="bank-block">
                        <div className="bank-text">
                            <h6 className="a-title txt-pdn">Bank account</h6>
                            <p className="a-subtitle">Automatically import cleared transactions
                                from your personal bank account.
                            </p>
                        </div>
                        <div className="account-img img-lf-mg">
                            <i className="fas fa-university"/>
                        </div>
                    </div>
                </label>
                <input
                    type="radio"
                    id="radio-2"
                    style={{display: 'none'}}
                    onChange={(e) => setChecked(e.target.value)}
                    value="Manual account"
                    name="radios"
                />
                <label className="wrap" htmlFor="radio-2">
                    <div className="manual-block">
                        <div className="account-img">
                            <i className="fas fa-hand-pointer"/>
                        </div>
                        <div className="manual-text">
                            <h6 className="a-title txt-pdn">Bank account</h6>
                            <p className="a-subtitle">Setup a current ballance and get total
                                control by manually adding transactions.
                            </p>
                        </div>
                    </div>
                </label>
            </div>
            <button data-direction="next"
                    disabled={!checked}
                    className="button btn-green btn-mg-top"
                    onClick={props.onNextSlide}
            >Continue
            </button>
        </div>
    )
}

let mapStateToProps = (state) => {
    return ({
        indexSlide: state.slideReducer.indexSlide,
    })
}
let mapDispatchToProps = (dispatch) => ({
    changeSlide: index => {
        dispatch(getIndex(index))
    },
})

export default connect(mapStateToProps, mapDispatchToProps)(AddingAccount);