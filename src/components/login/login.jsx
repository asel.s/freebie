import React, {useEffect, useState} from "react";
import './login.css'
import {connect} from "react-redux";
import {getIndex} from "../../reducer/slide-reducer";


const Login = (props) => {
    console.log(props.pinCode);
    const [passcode, setPasscode] = useState([]);

    const onSetCode = e => {
        setPasscode([...passcode, e.currentTarget.textContent]);
    }

    const handleBack = () => {
        props.changeSlide(props.indexSlide - 3);
    }

    useEffect(() => {
        let auth = passcode.length === props.pinCode.length && passcode.every(function(value, index)
            { return value === props.pinCode[index]});
            if(auth === true) {
                props.changeSlide(props.indexSlide + 1)
            } else {
                props.changeSlide(props.indexSlide)
            }
    }, [passcode]);

    return <div>
        <div className="wrapper">
            <div className="password-block">
                <h5 className="pass-title">Budget Planner</h5>
                <div className="pass-dots">
                    <div className={passcode[0] ? 'pass-dots__item' : 'disactive'}></div>
                    <div className={passcode[1] ? 'pass-dots__item' : 'disactive'}></div>
                    <div className={passcode[2] ? 'pass-dots__item' : 'disactive'}></div>
                    <div className={passcode[3] ? 'pass-dots__item' : 'disactive'}></div>
                </div>
                <div className="block-info">
                    <p className="block-info__item ops-txt">Unlocking with passcode</p>
                    <p className="block-info__item blc-txt">Use your fingerprint</p>
                </div>
            </div>
        </div>
        <div className="password-input-block">
            <div className="row">
                <div onClick={onSetCode} className="btn-num btn-blc">1</div>
                <div onClick={onSetCode} className="btn-num btn-blc">2</div>
                <div onClick={onSetCode} className="btn-num btn-blc">3</div>
            </div>
            <div className="row">
                <div onClick={onSetCode} className="btn-num btn-blc">4</div>
                <div onClick={onSetCode} className="btn-num btn-blc">5</div>
                <div onClick={onSetCode} className="btn-num btn-blc">6</div>
            </div>
            <div className="row">
                <div onClick={onSetCode} className="btn-num btn-blc">7</div>
                <div onClick={onSetCode} className="btn-num btn-blc">8</div>
                <div onClick={onSetCode} className="btn-num btn-blc">9</div>
            </div>
            <div className="row">
                <div className="btn-num btn-blc frg" onClick={handleBack}>forgot?</div>
                <div onClick={onSetCode} className="btn-num btn-blc">0</div>
                <div className="btn-num btn-blc"><i className="fas fa-fingerprint"/></div>
            </div>
        </div>
    </div>

}

let mapStateToProps = state => {
    return ({
        pinCode: state.signUpReducer.pinCode,
        indexSlide: state.slideReducer.indexSlide,
    })
}

let mapDispatchToProps = (dispatch) => ({
    changeSlide: index => {
        dispatch(getIndex(index))
    },
})

export default connect(mapStateToProps, mapDispatchToProps)(Login);
