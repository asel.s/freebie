import React from "react";
import './button.css';

const Button = ({onNextSlide}) => {
    return  (
        <div className="block-btn">
            <button data-direction="next" onClick={onNextSlide} className="button btn-green">Continue</button>
        </div>
    )
}

export default Button;