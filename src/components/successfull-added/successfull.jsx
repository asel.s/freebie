import React from "react";
import './successfull.css'


const Successfull = (props) => {
    return (
        <div className="wrapper center-item">
            <div className="block-done">
                <i className='fa fa-check'/>

                <h1>Your account has been
                    successfully added!</h1>
                <p>Lorem ipsum dollor sit ament pallentesque
                    adeipiscing elit enim minim ven
                    ultricies eget, tempor sit amet, ante.</p>
            </div>

            <button className="button btn-green btn-top-abs">Done</button>
        </div>
    )
}

export default Successfull;