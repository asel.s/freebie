import React from "react";
import './confirmCode.css';
import './confirmCode.css'
import {connect} from "react-redux";
import {clearPinCode,  setPinCode} from "../../../reducer/signup-reducer";
import arrow from "../../../assets/images/back-arrow.png";


const CreateCode = (props) => {

    const onInputCode = (e) => {
        props.setPinCode(e.currentTarget.textContent);
    }
    const onRemoveCode = () => {
        // setPincode(pincode.slice(0, -1));
        props.clearPinCode();
    }
    const isEnabled = props.pincode.length === 4;

    return <div>
        <div className="wrapper">
            <div className="form-items">
                <div className="arrow-block">
                    <img className="arrow" src={arrow} alt="" data-direction="prev" onClick={props.onNextSlide}/>
                </div>
                <div className="input-from">
                    <h5 className="start">Create passcode</h5>
                    <div className="code-wrap">
                        <div className={props.pincode[0] ? 'code__item' : 'code__empty'}>{props.pincode[0]}</div>
                        <div className={props.pincode[1] ? 'code__item' : 'code__empty'}>{props.pincode[1]}</div>
                        <div className={props.pincode[2] ? 'code__item' : 'code__empty'}>{props.pincode[2]}</div>
                        <div className={props.pincode[3] ? 'code__item' : 'code__empty'}>{props.pincode[3]}</div>
                    </div>
                </div>
            </div>
        </div>
        <div className="phone-number bg-color">
            <div className="row row-1">
                <div onClick={onInputCode} className="btn-num btn-num-wh active">1</div>
                <div onClick={onInputCode} className="btn-num btn-num-wh active">2</div>
                <div onClick={onInputCode} className="btn-num btn-num-wh active">3</div>
            </div>
            <div className="row row-2">
                <div onClick={onInputCode} className="btn-num btn-num-wh active">4</div>
                <div onClick={onInputCode} className="btn-num btn-num-wh active">5</div>
                <div onClick={onInputCode} className="btn-num btn-num-wh active">6</div>
            </div>
            <div className="row row-3">
                <div onClick={onInputCode} className="btn-num btn-num-wh active">7</div>
                <div onClick={onInputCode} className="btn-num btn-num-wh active">8</div>
                <div onClick={onInputCode} className="btn-num btn-num-wh active">9</div>
            </div>
            <div className="row row-4">
                <div className="input-num"></div>
                <div onClick={onInputCode} value="0" className="btn-num btn-num-wh active">0</div>
                <div className="btn-num btn-num-wh active">
                    <i className="fas fa-backspace" onClick={onRemoveCode}/></div>
            </div>
            <div className="btn-item">
                <button data-direction="next"
                        disabled={!isEnabled}
                        onClick={props.onNextSlide}
                        className="button btn-green"
                        style={{background: isEnabled ? '' : '#ccc'}}>Continue
                </button>
            </div>
        </div>
    </div>
     }

        let mapStateToProps = (state) => {
        return ({
        pincode: state.signUpReducer.pinCode
     })
     }

    export default connect(mapStateToProps, {setPinCode, clearPinCode})(CreateCode);