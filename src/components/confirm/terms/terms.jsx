import React from "react";
import arrow from "../../../assets/images/back-arrow.png";
import './terms.css';

const Terms = (props) => {

    return (
        <div className="wrapper">
            <div className="form-items">
                <div className="arrow-block">
                    <img className="arrow" src={arrow} alt="" data-direction="prev" onClick={props.onNextSlide}/>
                </div>
                <div className="input-from">
                    <h5 className="start">One last thing</h5>
                    <p className="info">Terms of service</p>
                    <q className="desc">Lorem ipsum dolor sit amet, consectetur adipiscing
                        elit, sed do eiusmod tempor incididunt ut labore et
                        dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut
                        aliquip ex ea commodo consequat.

                        Duis aute irure dolor in reprehenderit in voluptate
                        velit esse cillum dolore eu fugiat nulla pariatur.
                        Excepteur sint occaecat cupidatat non proident,
                        sunt in culpa qui officia deserunt mollit anim id
                        est laborum.

                        Pellentesque habitant morbi tristique senectus et
                        netus et malesuada fames ac turpis egestas ipsum.
                        Vestibulum tortor quam, feugiat vitae, ultricies eget,
                        tempor sit amet, ante eu libero sit amet quam.

                        Aenean ultricies mi vitae est. Mauris placerat
                        eleifend leo. Quisque sit est et sapien ullamcorper.</q>
                    <div className="buttons">
                        <button data-direction="next" className="button btn-green" onClick={props.onNextSlide}>Agree</button>
                        <button className="button btn-red">Decline</button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Terms;