import React, {useEffect} from "react";
import './modal.css';
import {connect} from "react-redux";
import {setVerifyCode} from "../../../../reducer/signup-reducer";


const Modal = ({show, setShow, verifyCode, putVerifyCode, pincode}) => {
    const showHideClassName = show ? 'modal display-block' : 'modal display-none';

    // const fourdigitsrandom = String(Math.floor(1000 + Math.random() * 9000))

    // useEffect(() => {
    //     putVerifyCode(fourdigitsrandom.split(''));
    // }, []);

    return (
        <div className={showHideClassName}>
            <section className="modal-main">
                <p>Catch your code - {pincode}</p>
                <button className="button btn-small" onClick={() => setShow(false)}>close</button>
            </section>
        </div>
    )
}

export default connect(null, {setVerifyCode})(Modal);