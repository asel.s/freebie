import React, {useEffect, useState} from "react";
import arrow from "../../../assets/images/back-arrow.png";
import './verify.css';
import {connect} from "react-redux";
import {setVerifyCode} from "../../../reducer/signup-reducer";
import Modal from "./modal-view/modal-view";
import {getIndex} from "../../../reducer/slide-reducer";


const Verification = (props) => {

    console.log(props.verifyCode);

    const [show, setShow] = useState(false);
    const [code, setCode] = useState([]);

    useEffect(() => {
        setTimeout(() => {
            setShow(true)
        }, 1500);
    }, []);

    const onSetCode = (e) => {
        setCode([...code, e.currentTarget.textContent])
    }
    const onRemoveCode = () => {
        setCode(code.slice(0, -1));
    }

    let result = code.length === props.pincode.length && code.every(function (value, index) {
        return value === props.pincode[index]
    });

    useEffect(() => {
        if (result === true) {
            props.changeSlide(props.indexSlide + 1);
        } else {
            props.changeSlide(props.indexSlide);
        }
    }, [code]);

    return <div>
        <div className="wrapper">
            <div className="form-items">
                <div className="arrow-block">
                    <img className="arrow" src={arrow} alt="" data-direction="prev" onClick={props.onNextSlide}/>
                </div>
                <div className="input-from">
                    <h5 className="start">Verification code</h5>
                    <p className="info">Please enter the 4-digit code sent to you
                        at {props.phoneNumber}</p>
                </div>
                <Modal show={show} setShow={setShow}
                       putVerifyCode={props.putVerifyCode}
                       verifyCode={props.verifyCode}
                       pincode={props.pincode}
                />
            </div>
            <div className="setCode-block">
                <div className="verify-code">
                    <div className={code[1] && 'code' || code[0] && 'code-success'}>
                        {code[0]}
                    </div>
                    <div className={code[2] && 'code' || code[1] && 'code-success'}>
                        {code[1]}
                    </div>
                    <div className={code[3] && 'code' || code[2] && 'code-success'}>
                        {code[2]}
                    </div>
                    <div className={code[4] && 'code' || code[3] && 'code-success'}>
                        {code[3]}
                    </div>
                </div>
                <div className="receive-item">
                    <p className="receive-item__text">If you don’t receive the code in 30 seconds
                        tap bellow to resend it
                    </p>
                </div>
            </div>
        </div>
        <div className="keypad-block">
            <div className="code-block">
                <div className="row row-1">
                    <div onClick={onSetCode} className="btn-num btn-num-wh  active">1</div>
                    <div onClick={onSetCode} className="btn-num btn-num-wh active">2</div>
                    <div onClick={onSetCode} className="btn-num btn-num-wh active">3</div>
                </div>
                <div className="row">
                    <div onClick={onSetCode} className="btn-num btn-num-wh  active">4</div>
                    <div onClick={onSetCode} className="btn-num btn-num-wh  active">5</div>
                    <div onClick={onSetCode} className="btn-num btn-num-wh  active">6</div>
                </div>
                <div className="row">
                    <div onClick={onSetCode} className="btn-num btn-num-wh active">7</div>
                    <div onClick={onSetCode} className="btn-num btn-num-wh active">8</div>
                    <div onClick={onSetCode} className="btn-num btn-num-wh active">9</div>
                </div>
                <div className="row">
                    <div className="btn-num active"></div>
                    <div onClick={onSetCode} value="0" className="btn-num btn-num-wh active">0</div>
                    <div onClick={onRemoveCode} className="btn-num btn-num-wh active"><i className="fas fa-backspace"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
}
let mapStateToProps = (state) => {
    return ({
        indexSlide: state.slideReducer.indexSlide,
        verifyCode: state.signUpReducer.verifyCode,
        phoneNumber: state.signUpReducer.phone,
        pincode: state.signUpReducer.pinCode
    })
};

let mapDispatchToProps = (dispatch) => ({
    changeSlide: index => {
        console.log(index);
        dispatch(getIndex(index))
    },
    putVerifyCode: code => {
        console.log(code);
        dispatch(setVerifyCode(code))
    }

})

export default connect(mapStateToProps, mapDispatchToProps)(Verification);
