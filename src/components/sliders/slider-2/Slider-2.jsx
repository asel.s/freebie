import React from "react";
import './slider-2.css';
import Dots from "../../../dots/dots";
import body from '../../../assets/images/body.png';
import dashBoard from '../../../assets/images/Dashboard.png';

const SecondSlider = (props) => {
    return <div className="second-slide">

        <h1 className="slide-title title-pad-2">Welcome to Budget Planner</h1>
            <h6 className="slide-subtitle">Take control of your money by tracking
                your expenses, adding goals
            </h6>
        <div className="group-image">
            <img src={body} alt=""/>
            <img className="dashBoard" src={dashBoard} alt=""/>
        </div>
        <div className="btn-dots-block">
            <div className="btn-skip" onClick={props.onNextSlide} data-direction="next">
                <span className="toBack">Skip</span>
            </div>
            <Dots className="dot-items"/>
            <button className="button btn-arrow" data-direction="next" onClick={props.onNextSlide}>
                <i className="fas fa-long-arrow-alt-right"/>
            </button>
        </div>

    </div>
}

export default SecondSlider;


