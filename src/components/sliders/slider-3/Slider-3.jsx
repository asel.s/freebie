import React from "react";
import './slider3.css';
import Dots from "../../../dots/dots";
import lightPic from '../../../assets/images/fs.png';
import darkPic  from '../../../assets/images/tw.png';
import {connect} from "react-redux";
import {getIndex} from "../../../reducer/slide-reducer";
import {setVerifyCode} from "../../../reducer/signup-reducer";

const ThirdSlider = (props) => {

    const handleToLogin = () => {
        props.changeSlide(props.indexSlide + 3)
    }
    return <div className="third-slide">
        <h1 className="slide-title title-pad-2">Light and Dark theme</h1>
        <div className="block-theme">
            <img src={lightPic} alt=""/>
            <img src={darkPic} alt=""/>
        </div>
        <p className="slide-subtitle-sz">Take control of your money by tracking
            your expenses, adding goals</p>
        <Dots className="dot-pd-top"/>
        <div className="button-group">
            <button className="btn login" onClick={handleToLogin}>Login</button>
            <button className="btn sign" data-direction="next" onClick={props.onNextSlide}>Sign up</button>
        </div>
    </div>
}

let mapStateToProps = (state) => {
    return ({
        indexSlide: state.slideReducer.indexSlide,
    })
};

let mapDispatchToProps = (dispatch) => ({
    changeSlide: index => {
        dispatch(getIndex(index))
    },
})

export default connect(mapStateToProps, mapDispatchToProps)(ThirdSlider);