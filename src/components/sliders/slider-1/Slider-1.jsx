import React from "react";
import './slider-1.css';
import Dots from "../../../dots/dots";
import one from '../../../assets/images/1.png';
import two from '../../../assets/images/2.png';
import three from '../../../assets/images/3.png';


const FirstSlider = ({onNextSlide}) => {

    return <div className="first-slide">
        <div className="slide-img">
            <img src={one} alt=""/>
            <img src={two} alt=""/>
            <img src={three} alt=""/>
        </div>
        <h1 className="slide-title title-pad-1">Let’s get started</h1>
        <h6 className="slide-subtitle">Take control of your money by tracking your expenses, adding goals</h6>
        <Dots onNextSlide={onNextSlide} className="dot-pd-top"/>
        <button className="button btn-white top" data-direction="next" onClick={onNextSlide}>
            NEXT
        </button>
    </div>
}

export default FirstSlider;