
let initialState = {
    indexSlide: 1
}

const slideReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'ADD_INDEX':
            return {
                ...state,
                indexSlide: action.payload
            }
        default:
            return state;
    }
};

export const getIndex = (index) => ({type: 'ADD_INDEX', payload: index});


export default slideReducer;