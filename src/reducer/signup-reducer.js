const INPUT_PHONE_NUMBER = 'INPUT_PHONE_NUMBER';
const CLEAR_PHONE_NUMBER = 'CLEAR_PHONE_NUMBER';
const SET_PINCODE = 'SET_PINCODE';
const GET_VERIFY_CODE = 'GET_VERIFY_CODE';
const CLEAR_VERIFY_CODE = 'CLEAR_VERIFY_CODE';
const CLEAR_PINCODE = 'CLEAR_PINCODE';

let InitialState = {
    phone: ['996'],
    pinCode: [],
    verifyCode: [],
};


const signupReducer = (state = InitialState, action) => {
    switch (action.type) {
        case INPUT_PHONE_NUMBER:
            return {
                ...state,
                phone:  [...state.phone, action.number]
            }
        case  CLEAR_PHONE_NUMBER:
            return {
                ...state,
                phone: [...state.phone.slice(0, -1)]
            }
        case SET_PINCODE:
            console.log(action.pincode)
            return {
                ...state,
                pinCode: [...state.pinCode, action.pincode]
            }

        case CLEAR_PINCODE:
            return {
                ...state,
                pinCode: [...state.pinCode.slice(0, -1)]
            }
        case GET_VERIFY_CODE:
            return {
                ...state,
                verifyCode: action.verifyCode
            }
        case CLEAR_VERIFY_CODE:
            return {
                ...state,
                verifyCode: [...state.verifyCode.slice(0, -1)]
            }
        default:
            return state;

    }
}

export const setPhoneNumber = (number) => ({type: INPUT_PHONE_NUMBER, number});

export const clearedPhoneNumber = () => ({type: CLEAR_PHONE_NUMBER});

export const setPinCode = (pincode) => ({type: SET_PINCODE, pincode});

export const clearPinCode = (index) => ({type: CLEAR_PINCODE, index});

export const setVerifyCode = (verifyCode) => ({type: GET_VERIFY_CODE, verifyCode});

export const clearVerifyCode = () => ({type: CLEAR_VERIFY_CODE});


export default signupReducer;

