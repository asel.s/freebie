import React from "react";
import './dots.css';
import {connect} from "react-redux";
import {getIndex} from "../reducer/slide-reducer";


const Dots = (props) => {
    const onFirstSlider = () => {
        if(props.indexSlide === 1) {
            props.getIndex(props.indexSlide - 1)
        } else if(props.indexSlide === 2) {
            props.getIndex(props.indexSlide - 2)
        }
    }
    const onSecondSlider = () => {
        if (props.indexSlide === 0) {
            props.getIndex(props.indexSlide + 1)
        }
        else if (props.indexSlide === 2) {
            props.getIndex(props.indexSlide - 1)
        }
    }
    const onThirdSlider = () => {
        if(props.indexSlide === 0) {
            props.getIndex(props.indexSlide + 2)
        } else if(props.indexSlide === 1)
        props.getIndex(props.indexSlide + 1)
    }

    return <div className="Dots">
        <div onClick={onFirstSlider} data-direction="prev" className={props.indexSlide === 0 ? 'active-dot' : 'dot'}></div>
        <div onClick={onSecondSlider} data-direction="next" className={props.indexSlide === 1 ? 'active-dot' : 'dot'}></div>
        <div onClick={onThirdSlider} className={props.indexSlide === 2 ? 'active-dot' : 'dot'}></div>
    </div>
}

let mapStateToProps = (state) => {
    return ({
        indexSlide: state.slideReducer.indexSlide
    })
}

export default connect(mapStateToProps, {getIndex})(Dots);